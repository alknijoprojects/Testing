import { Via } from './via';

export interface information {
  id: number;
  nameAgent: string;
  direction: string;
  codeAgent: string;
  via: Via;
}
