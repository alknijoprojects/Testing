export class NewUser {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  password: string;
  constructor(
    firstname: string,
    lastname: string,
    username: string,
    email: string,
    password: string
  ) {
    this.firstName = firstname;
    this.lastName = lastname;
    this.username = username;
    this.email = email;
    this.password = password;
  }
}
