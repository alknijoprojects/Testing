import { Via } from './via';

export interface Agent {
  id: number;
  code: string;
  name: string;
  lastname: string;
  years: number;
  codeAgent: string;
  via: Via;
}
