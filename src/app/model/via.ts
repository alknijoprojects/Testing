export interface Via {
  id: number;
  type: string;
  sense: string;
  number: number;
  congestionNumber: number;
}
