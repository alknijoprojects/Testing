import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { TokenService } from '../services/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  routerlink: RouterLink | unknown;
  isLogged = false;
  isAdmin = false;
  constructor(private router: Router, private tokenService: TokenService) {}
  ngOnInit() {
    this.isLogged = this.tokenService.isLogged();
  }
  onLogOut(): void {
    this.tokenService.logOut();
  }
}
