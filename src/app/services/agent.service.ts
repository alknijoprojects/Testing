import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Agent } from '../model/agent';
import { environment } from 'src/environments/environment';
import { map, first } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { throwError as obeservableThrowError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AgentService {
  private apiServerUrl = environment.apiBaseUrl + '/api/agent';

  constructor(private http: HttpClient) {}

  public getAgents(): Observable<Agent[]> {
    first();
    return this.http
      .get<Agent[]>(`${this.apiServerUrl}`)
      .pipe(map(this.transformAgentsIntoAgent));
  }

  private errorHandler(error: HttpErrorResponse) {
    return obeservableThrowError(error.message);
  }
  public getAgentById(agentId: number): Observable<Agent> {
    return this.http.get<Agent>(`${this.apiServerUrl}/${agentId}`);
  }

  public assignViaInTheAgent(viaId: number, agent: Agent): Observable<Agent> {
    return this.http.put<Agent>(`${this.apiServerUrl}/asignar/${viaId}`, agent);
  }

  public addAgent(agent: Agent): Observable<Agent> {
    return this.http
      .post<Agent>(`${this.apiServerUrl}`, agent)
      .pipe(catchError(this.errorHandler));
  }

  public updateAgent(agentId: number, agent: Agent): Observable<Agent> {
    return this.http.put<Agent>(`${this.apiServerUrl}/${agentId}`, agent);
  }

  public deleteAgent(agentId: number): Observable<Agent> {
    return this.http.delete<Agent>(`${this.apiServerUrl}/${agentId}`);
  }

  private transformAgentsIntoAgent(resp: Agent[]): Agent[] {
    return resp;
  }
}
