import { Injectable } from '@angular/core';
import { catchError, first, map, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Via } from '../model/via';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ViaService {
  private apiServerUrl = environment.apiBaseUrl + '/api/Via';

  constructor(private http: HttpClient) {}

  public getVias(): Observable<Via[]> {
    first();
    return this.http
      .get<Via[]>(`${this.apiServerUrl}`)
      .pipe(map(this.transformViastoVia));
  }

  public getViaById(viaId: number): Observable<Via> {
    return this.http.get<Via>(`${this.apiServerUrl}/${viaId}`);
  }

  public addVia(via: Via): Observable<Via> {
    return this.http
      .post<Via>(`${this.apiServerUrl}`, via)
      .pipe(catchError(this.errorHandler));
  }

  public updateVia(viaId: number, via: Via): Observable<Via> {
    return this.http.put<Via>(`${this.apiServerUrl}/${viaId}`, via);
  }

  public deleteVia(viaId: number): Observable<Via> {
    return this.http.delete<Via>(`${this.apiServerUrl}/${viaId}`);
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(() => error.message);
  }

  private transformViastoVia(resp: Via[]): Via[] {
    return resp;
  }
}
