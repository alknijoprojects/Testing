import { Injectable } from '@angular/core';
import { first, map, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { throwError } from 'rxjs';
import { information } from '../model/information';

@Injectable({
  providedIn: 'root',
})
export class InformationService {
  private apiServerUrl = environment.apiBaseUrl + '/api/Information';

  constructor(private http: HttpClient) {}

  public getInfo(): Observable<information[]> {
    first();
    return this.http
      .get<information[]>(`${this.apiServerUrl}`)
      .pipe(map(this.transformInfosToInfo));
  }

  public getInfoById(infoId: number): Observable<information> {
    return this.http.get<information>(`${this.apiServerUrl}/${infoId}`);
  }

  public deleteInfo(infoId: number): Observable<information> {
    return this.http.delete<information>(`${this.apiServerUrl}/${infoId}`);
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message);
  }

  private transformInfosToInfo(resp: information[]): information[] {
    return resp;
  }
}
