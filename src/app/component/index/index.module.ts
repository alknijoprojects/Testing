import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexRoutingModule } from './index-routing.module';
import { IndexComponent } from './index.component';
import { ComponentsModuleModule } from 'src/app/header/components-module.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [IndexComponent],
  imports: [
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    IndexRoutingModule,
    ComponentsModuleModule,
    CommonModule,
    NgbModule,
  ],
})
export class IndexModule {}
