import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformationRoutingModule } from './information-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { InformationComponent } from './informationComponent/information.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModuleModule } from 'src/app/header/components-module.module';

@NgModule({
  declarations: [InformationComponent],
  imports: [
    CommonModule,
    InformationRoutingModule,
    MaterialModule,
    FormsModule,
    NgbModule,
    ComponentsModuleModule,
  ],
})
export class InformationModule {}
