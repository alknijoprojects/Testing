import { HttpErrorResponse } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { information } from 'src/app/model/information';
import { InformationService } from 'src/app/services/information.service';
import { InformationModule } from '../information.module';

import { InformationComponent } from './information.component';

describe('InformationComponent', () => {
  let component: InformationComponent;
  let fixture: ComponentFixture<InformationComponent>;
  let InformationServiceSpy: jasmine.SpyObj<InformationService>;
  const informationMock: information = {
    id: 70004,
    nameAgent: 'CAMILO',
    direction: 'Carrera151',
    codeAgent: 'F001',
    via: {
      id: 1,
      type: 'Carretera Principal',
      sense: 'Calle',
      number: 154,
      congestionNumber: 36.9,
    },
  };
  beforeEach(async () => {
    InformationServiceSpy = jasmine.createSpyObj<InformationService>(
      'InformationService',
      ['getInfo', 'getInfoById', 'deleteInfo']
    );
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, InformationModule],
      declarations: [InformationComponent],
      providers: [
        { provide: InformationService, useValue: InformationServiceSpy },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationComponent);
    component = fixture.componentInstance;
    InformationServiceSpy.getInfo.and.returnValue(of([informationMock]));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('testing onDeleteInfo', function (DoneFn) {
    InformationServiceSpy.deleteInfo.and.returnValue(of(informationMock));
    component.onDeleteInfo(informationMock.id);
    expect(component.error).toEqual(false);
    const errorResponse = new HttpErrorResponse({
      error: 'test 404 internal Server',
      status: 404,
      statusText: 'Not Found',
    });
    InformationServiceSpy.deleteInfo.and.returnValue(
      throwError(() => {
        const error: any = errorResponse;
        return error;
      })
    );
    component.onDeleteInfo(informationMock.id);
    expect(component.error).toEqual(true);
    DoneFn();
  });
  it('testing search', function (DoneFn) {
    const info: information[] = [
      {
        id: 70004,
        nameAgent: 'CAMILO',
        direction: 'Carrera151',
        codeAgent: 'F001',
        via: {
          id: 1,
          type: 'Carretera Principal',
          sense: 'Calle',
          number: 154,
          congestionNumber: 36.9,
        },
      },
      {
        id: 70002,
        nameAgent: 'DANILO',
        direction: 'Carrera151',
        codeAgent: 'F002',
        via: {
          id: 1,
          type: 'Carretera Principal',
          sense: 'Calle',
          number: 154,
          congestionNumber: 36.9,
        },
      },
    ];
    InformationServiceSpy.getInfo.and.returnValue(of(info));
    component.onSearch('F002');
    expect(component.collectionSize).toBe(1);
    DoneFn();
  });
  it('testing onOpenModal', function (DoneFn) {
    component.onOpenModal(informationMock, 'delete');
    expect(component.deleteInfo).toEqual(informationMock);
    DoneFn();
  });
});
