import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { information } from 'src/app/model/information';
import { InformationService } from 'src/app/services/information.service';
import { TokenService } from 'src/app/services/token.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css'],
})
export class InformationComponent implements OnInit {
  // creacion de variables
  isAdmin = false;
  public infos: information[] = [];
  public testInfo: information | undefined;
  public error: boolean | undefined;
  public infoFilter: information[] = [];
  public editInfo: information | undefined;
  public deleteInfo: information | undefined;
  //variable para buscar
  public search = '';

  constructor(
    private informationService: InformationService,
    private tokenService: TokenService
  ) {}
  // variables paginacion
  page = 0;
  pageSize = 5;
  collectionSize = 0;
  page_number = 2;
  pageSizeOptions = [6];
  ngOnInit(): void {
    this.getInfo();
    this.isAdmin = this.tokenService.isAdmin();
  }
  //funciones con la base de datos
  public getInfo(): void {
    this.informationService.getInfo().subscribe(response => {
      this.infos = response;
      this.collectionSize = this.infos.length;
      this.infos = this.infos
        .map((info, i) => ({ counter: i + 1, ...info }))
        .slice(
          (this.page - 1) * this.pageSize,
          (this.page - 1) * this.pageSize + this.pageSize
        );
    });
  }
  private getAllInfo(): void {
    this.informationService.getInfo().subscribe(response => {
      this.infoFilter = response;
    });
  }
  public onDeleteInfo(infoId: number | any): void {
    this.informationService.deleteInfo(infoId).subscribe({
      next: () => {
        this.error = false;
        this.getInfo();
      },
      error: (error: HttpErrorResponse) => {
        this.error = true;
        Swal.fire('Error!', error.message, 'error');
      },
    });
  }
  //funciones para la pagina
  public onSearch(search: string) {
    this.search = search;
    this.getAllInfo();
    //revisar el filtro de via
    const filteredVia = this.infoFilter.filter(
      inf =>
        inf.id.toString().includes(search) ||
        inf.codeAgent?.includes(search) ||
        inf.via.id.toString().includes(search) ||
        inf.nameAgent?.includes(search)
    );
    this.collectionSize = filteredVia.length;
    if (this.search === '') {
      return this.getInfo();
    } else {
      this.infos = filteredVia;
      this.infos = this.infos
        .map((via, i) => ({ counter: i + 1, ...via }))
        .slice(
          (this.page - 1) * this.pageSize,
          (this.page - 1) * this.pageSize + this.pageSize
        );
      return filteredVia;
    }
  }

  public onOpenModal(info: information, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (mode === 'delete') {
      this.deleteInfo = info;
      button.setAttribute('data-target', '#deleteInfoModal');
    }
    container?.appendChild(button);
    button.click();
  }
}
