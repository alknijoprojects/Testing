import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Agent } from 'src/app/model/agent';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { AgentService } from 'src/app/services/agent.service';
import { ViaService } from 'src/app/services/via.service';
import { Via } from 'src/app/model/via';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.css'],
})
export class AgentComponent implements OnInit {
  isAdmin = false;
  //variables
  public agents: Agent[] = [];
  public agentFilter: Agent[] = [];
  public editAgent: Agent | undefined;
  public editVia: Via | undefined;
  public deleteAgent: Agent | undefined;
  public error: boolean | undefined;
  public id = 0;
  public vias: Via[] = [];
  //variables select
  opcionSeleccionado = 0;
  verSeleccion = 0;
  //variables buscar
  public search = '';
  //variable error
  public errorMessage = '';
  //variable FormGroup
  myGroup!: FormGroup;
  //variables paginacion
  page = 0;
  pageSize = 5;
  collectionSize = 0;
  page_number = 2;
  pageSizeOptions = [6];

  constructor(
    private agentService: AgentService,
    private viaService: ViaService,
    private formBuilder: FormBuilder,
    private tokenSerivce: TokenService
  ) {
    //  creacion del grupo con formBuilder.
    this.myGroup = this.formBuilder.group({
      id: [],
      code: [''],
      name: [''],
      lastname: [''],
      years: [],
      codeAgent: [''],
      via: [],
    });
  }

  ngOnInit(): void {
    this.getAgents();
    this.isAdmin = this.tokenSerivce.isAdmin();
  }
  // Funciones con el Servicio.
  public getAgents(): void {
    this.agentService.getAgents().subscribe(response => {
      this.agents = response;
      this.collectionSize = this.agents.length;
      this.agents = this.agents
        .map((agent, i) => ({ counter: i + 1, ...agent }))
        .slice(
          (this.page - 1) * this.pageSize,
          (this.page - 1) * this.pageSize + this.pageSize
        );
    });
  }
  public getAllAgents(): void {
    this.agentService.getAgents().subscribe(response => {
      this.agentFilter = response;
    });
  }
  public getVias(): Via[] {
    this.viaService.getVias().subscribe(response => {
      this.vias = response;
    });
    return this.vias;
  }

  onSearchAgent(search: string) {
    this.search = search;
    this.getAllAgents();
    const filteredAgents = this.agentFilter.filter(
      agent => agent.code.includes(search) || agent.name.includes(search)
    );

    this.collectionSize = filteredAgents.length;
    if (this.search === '') {
      return this.getAgents();
    } else {
      this.agents = filteredAgents;
      this.agents = this.agents
        .map((agent, i) => ({ counter: i + 1, ...agent }))
        .slice(
          (this.page - 1) * this.pageSize,
          (this.page - 1) * this.pageSize + this.pageSize
        );
      return filteredAgents;
    }
  }

  public onAddAgent(agent: Agent): void {
    const agentform = document.getElementById('update-agent-form');
    agentform?.click();
    this.agentService.addAgent(agent).subscribe({
      next: () => {
        this.getAgents();
        this.error = false;
        Swal.fire('Confirmado', 'Agente Registrado Exitosamente', 'success');
        this.myGroup.reset();
      },
      error: (error: HttpErrorResponse) => {
        this.error = true;
        Swal.fire('Error!', error.message, 'error');
        this.myGroup.reset();
      },
    });
  }

  public onDeleteAgent(agentId: number | any): void {
    this.agentService.deleteAgent(agentId).subscribe({
      next: () => {
        Swal.fire(
          'Confirmado',
          'El Agente se ha eliminado con exito',
          'success'
        );
        this.error = false;
        this.getAgents();
      },
      error: (error: HttpErrorResponse) => {
        this.error = true;
        Swal.fire('Error!', error.message, 'error');
      },
    });
  }

  public onUpdateAgent(agent: Agent) {
    this.agentService.updateAgent(agent.id, agent).subscribe({
      next: (response: any) => {
        Swal.fire(
          'Confirmado',
          'El usuario ' + response.id + ' se actualizo con exito',
          'success'
        );
        this.getAgents();
        this.error = false;
        this.myGroup.reset();
      },
      error: (error: HttpErrorResponse) => {
        Swal.fire('Error!', error.message, 'error');
        this.error = true;
      },
    });
  }

  public onUpdateViaAgent(agent: Agent, dato: number): void {
    this.verSeleccion = dato;
    this.agentService.getAgentById(agent.id).subscribe(response => {
      agent = response;
      this.agentService
        .assignViaInTheAgent(this.verSeleccion, agent)
        .subscribe({
          next: (resp: any) => {
            Swal.fire(
              'Éxito!',
              'El agente ' + resp.code + ' tiene la via id: ' + resp.via.id,
              'success'
            );
            this.error = false;
            this.getAgents();
          },
          error: (error: HttpErrorResponse) => {
            if (error.status == 403) {
              Swal.fire(
                'Fallo!',
                'La congestion de la via es inferior a 30',
                'error'
              );
              this.error = true;
            }
            if (error.status == 402) {
              Swal.fire(
                'Fallo!',
                'El agente ya ha tenido 3 vias asignadas',
                'error'
              );
              this.error = true;
            }
            if (error.status == 401) {
              Swal.fire(
                'Fallo!',
                'La petición es rechazada por el servidor',
                'error'
              );
              this.error = true;
            }
          },
        });
    });
  }

  //funcion con el modal.
  public onOpenModal(agent: Agent, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      this.error = false;
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.id = agent.id;
      this.editAgent = agent;
      button.setAttribute('data-target', '#updateAgentModal');
    }
    if (mode === 'via') {
      this.editAgent = agent;
      this.vias = this.getVias();
      button.setAttribute('data-target', '#viaAgentModal');
    }
    if (mode === 'delete') {
      this.deleteAgent = agent;
      button.setAttribute('data-target', '#deleteAgentModal');
    }
    container?.appendChild(button);
    button.click();
  }
}
