import { HttpErrorResponse } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { Agent } from 'src/app/model/agent';
import { Via } from 'src/app/model/via';
import { AgentService } from 'src/app/services/agent.service';
import { ViaService } from 'src/app/services/via.service';
import { AgentModule } from '../agent.module';
import { AgentComponent } from './agent.component';

describe('AgentComponent', () => {
  let component: AgentComponent;
  let fixture: ComponentFixture<AgentComponent>;
  let agentServiceSpy: jasmine.SpyObj<AgentService>;
  let viaServiceSpy: jasmine.SpyObj<ViaService>;

  const errorResponse = new HttpErrorResponse({
    error: 'test 404 internal Server',
    status: 404,
    statusText: 'Not Found',
  });
  const agentsMock: Agent[] = [
    {
      id: 10002,
      code: 'F009',
      name: 'CAMILO',
      lastname: 'PEREZ',
      years: 14.0,
      codeAgent: 'CTX01',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    },
    {
      id: 40002,
      code: 'F011',
      name: 'DANIEL',
      lastname: 'PEREZ',
      years: 14.0,
      codeAgent: 'CTX01',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    },
    {
      id: 50002,
      code: 'RF01',
      name: 'RICARDO',
      lastname: 'RODRIGUEZ',
      years: 30.5,
      codeAgent: 'XCZ123',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    },
  ];
  const viaMock: Via[] = [
    {
      id: 1,
      type: 'Carretera Principal',
      sense: 'Calle',
      number: 154,
      congestionNumber: 36.9,
    },
  ];
  beforeEach(waitForAsync(() => {
    agentServiceSpy = jasmine.createSpyObj<AgentService>('AgentService', [
      'getAgents',
      'getAgentById',
      'assignViaInTheAgent',
      'addAgent',
      'updateAgent',
      'deleteAgent',
    ]);
    viaServiceSpy = jasmine.createSpyObj<ViaService>('ViaService', [
      'getVias',
      'addVia',
      'updateVia',
      'deleteVia',
    ]);
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, AgentModule],
      declarations: [AgentComponent],
      providers: [
        { provide: AgentService, useValue: agentServiceSpy },
        { provide: ViaService, useValue: viaServiceSpy },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentComponent);
    component = fixture.componentInstance;
    agentServiceSpy.getAgents.and.returnValue(of(agentsMock));
    fixture.detectChanges();
  });
  it('should create, testing getAllAgents and getAgents', () => {
    expect(component).toBeTruthy();
  });
  it('testing getVias on agentComponent', function (DoneFn) {
    viaServiceSpy.getVias.and.returnValue(of(viaMock));
    component.getVias();
    expect(component.vias).toEqual(viaMock);
    DoneFn();
  });
  it('testing onSearchAgent', function (DoneFn) {
    agentServiceSpy.getAgents.and.returnValue(of(agentsMock));
    component.onSearchAgent('RF01');
    expect(component.collectionSize).toBe(1);
    DoneFn();
  });
  it('testing onAddAgent', function (DoneFn) {
    const form = component.myGroup;
    form.controls['id'].setValue(1);
    form.controls['code'].setValue('RF01');
    form.controls['name'].setValue('Ricardo');
    form.controls['lastname'].setValue('Rodriguez');
    form.controls['years'].setValue(11);
    form.controls['codeAgent'].setValue('TR01');
    agentServiceSpy.addAgent.and.returnValue(of(form.value));
    component.onAddAgent(form.value);
    expect(component.error).toBeFalsy();
    agentServiceSpy.addAgent.and.returnValue(
      throwError(() => {
        const error: any = errorResponse;
        return error;
      })
    );
    component.onAddAgent(form.value);
    expect(component.error).toBeTruthy();
    DoneFn();
  });
  it('testing onDeleteAgent', function (DoneFn) {
    const agent: Agent = {
      id: 10002,
      code: 'F009',
      name: 'CAMILO',
      lastname: 'PEREZ',
      years: 14.0,
      codeAgent: 'CTX01',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    };
    agentServiceSpy.deleteAgent.and.returnValue(of(agent));
    component.onDeleteAgent(agent.id);
    expect(component.error).toEqual(false);
    agentServiceSpy.deleteAgent.and.returnValue(
      throwError(() => {
        const error: any = errorResponse;
        return error;
      })
    );
    component.onDeleteAgent(agent.id);
    expect(component.error).toBeTruthy();
    DoneFn();
  });
  it('testing onUpdateAgent', function (DoneFn) {
    const agent: Agent = {
      id: 10002,
      code: 'F009',
      name: 'CAMILO',
      lastname: 'PEREZ',
      years: 14.0,
      codeAgent: 'CTX01',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    };
    agentServiceSpy.updateAgent.and.returnValue(of(agent));
    component.onUpdateAgent(agent);
    expect(component.error).toBeFalse();
    agentServiceSpy.updateAgent.and.returnValue(
      throwError(() => {
        const error: any = errorResponse;
        return error;
      })
    );
    component.onUpdateAgent(agent);
    expect(component.error).toBeTruthy();
    DoneFn();
  });
  it('testing onUpdateViaAgent', function (DoneFn) {
    const agent: Agent = {
      id: 10002,
      code: 'F009',
      name: 'CAMILO',
      lastname: 'PEREZ',
      years: 14.0,
      codeAgent: 'CTX01',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    };
    agentServiceSpy.getAgentById.and.returnValue(of(agent));
    agentServiceSpy.assignViaInTheAgent.and.returnValue(of(agent));
    component.onUpdateViaAgent(agent, 1);
    expect(component.error).toBeFalse();
    DoneFn();
  });
  it('testing all errors in the onUpdateViaAgent', function (DoneFn) {
    const agent: Agent = {
      id: 10002,
      code: 'F009',
      name: 'CAMILO',
      lastname: 'PEREZ',
      years: 14.0,
      codeAgent: 'CTX01',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    };
    const errorResponse1 = new HttpErrorResponse({
      error: 'test 401 internal Server',
      status: 401,
      statusText: 'Not Found',
    });
    agentServiceSpy.getAgentById.and.returnValue(of(agent));
    agentServiceSpy.assignViaInTheAgent.and.returnValue(
      throwError(() => {
        const error: any = errorResponse1;
        return error;
      })
    );
    component.onUpdateViaAgent(agent, 1);
    expect(component.error).toBeTrue();
    const errorResponse2 = new HttpErrorResponse({
      error: 'test 402 internal Server',
      status: 402,
      statusText: 'Not Found',
    });
    agentServiceSpy.getAgentById.and.returnValue(of(agent));
    agentServiceSpy.assignViaInTheAgent.and.returnValue(
      throwError(() => {
        const error: any = errorResponse2;
        return error;
      })
    );
    component.onUpdateViaAgent(agent, 1);
    expect(component.error).toBeTrue();
    const errorResponse3 = new HttpErrorResponse({
      error: 'test 403 internal Server',
      status: 403,
      statusText: 'Not Found',
    });
    agentServiceSpy.getAgentById.and.returnValue(of(agent));
    agentServiceSpy.assignViaInTheAgent.and.returnValue(
      throwError(() => {
        const error: any = errorResponse3;
        return error;
      })
    );
    component.onUpdateViaAgent(agent, 1);
    expect(component.error).toBeTrue();
    DoneFn();
  });
  it('testing Modal', function (DoneFn) {
    const agent: Agent = {
      id: 10002,
      code: 'F009',
      name: 'CAMILO',
      lastname: 'PEREZ',
      years: 14.0,
      codeAgent: 'CTX01',
      via: {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    };
    component.onOpenModal(agent, 'edit');
    expect(component.editAgent).toEqual(agent);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    component.onOpenModal(null!, 'add');
    expect(component.error).toBeFalse();
    component.onOpenModal(agent, 'delete');
    expect(component.deleteAgent).toEqual(agent);
    viaServiceSpy.getVias.and.returnValue(of(viaMock));
    component.onOpenModal(agent, 'via');
    expect(component.editAgent).toEqual(agent);
    expect(component.vias.length).toBeGreaterThan(0);
    DoneFn();
  });
});
