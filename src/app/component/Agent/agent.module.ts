import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { AgentComponent } from './agentComponent/agent.component';
import { AgentRoutingModule } from './agent-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModuleModule } from 'src/app/header/components-module.module';

@NgModule({
  declarations: [AgentComponent],
  imports: [
    CommonModule,
    AgentRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModuleModule,
    NgbModule,
  ],
})
export class AgentModule {}
