import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RegisterRoutingModule } from './register-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { ComponentsModuleModule } from 'src/app/header/components-module.module';
@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    ToastrModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RegisterRoutingModule,
    ComponentsModuleModule,
    NgbModule,
  ],
})
export class RegisterModule {}
