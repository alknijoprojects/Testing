import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  waitForAsync,
} from '@angular/core/testing';
import { ViaComponent } from './via.component';
import { ViaService } from 'src/app/services/via.service';
import { RouterTestingModule } from '@angular/router/testing';
import { ViaModule } from './via.module';
import { of, throwError } from 'rxjs';
import { Via } from 'src/app/model/via';
import { HttpErrorResponse } from '@angular/common/http';

describe('ViaComponent', () => {
  let fixture: ComponentFixture<ViaComponent>;
  let component: ViaComponent;
  let viaServiceSpy: jasmine.SpyObj<ViaService>;
  let originalTimeout: any;

  beforeEach(waitForAsync(() => {
    viaServiceSpy = jasmine.createSpyObj<ViaService>('ViaService', [
      'getVias',
      'addVia',
      'updateVia',
      'deleteVia',
    ]);
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, ViaModule],
      declarations: [ViaComponent],
      providers: [{ provide: ViaService, useValue: viaServiceSpy }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViaComponent);
    component = fixture.componentInstance;
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
    viaServiceSpy.getVias.and.returnValue(
      of([
        {
          id: 1,
          type: 'Carretera Principal',
          sense: 'Calle',
          number: 154,
          congestionNumber: 36.9,
        },
      ])
    );
    fixture.detectChanges();
  });

  it('Debe de existir el ViaComponent', () => {
    expect(component).toBeTruthy();
  });

  it('metodo getVias On init Test', () => {
    viaServiceSpy.getVias.and.returnValue(
      of([
        {
          id: 1,
          type: 'Carretera Principal',
          sense: 'Calle',
          number: 154,
          congestionNumber: 36.9,
        },
      ])
    );
    component.ngOnInit();
    expect(component.viaTest?.length).toBe(1);
  });
  it('testing getAllVias ', fakeAsync(() => {
    const vias: Via[] = [
      {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
      {
        id: 2,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    ];
    viaServiceSpy.getVias.and.returnValue(of(vias));
    component.getAllVias();
    expect(component.viaFilter.length).toBe(2);
  }));
  it('testing add new Via', () => {
    const form = component.viaGroup;
    form.controls['id'].setValue(1);
    form.controls['type'].setValue('Autopista');
    form.controls['sense'].setValue('Calle');
    form.controls['number'].setValue(121);
    form.controls['congestionNumber'].setValue(11.11);
    const vias: Via = {
      id: 1,
      type: 'Autopista',
      sense: 'Calle',
      number: 121,
      congestionNumber: 11.11,
    };
    viaServiceSpy.addVia.and.returnValue(of(form.value));
    component.onAddVia(form.value);
    expect(component.vvTest).toEqual(vias);
  });
  afterEach(function () {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });
  it('Error nueva via error', function (DoneFn) {
    const form = component.viaGroup;
    form.controls['id'].setValue(1);
    form.controls['type'].setValue('Autopista');
    form.controls['sense'].setValue('Calle');
    form.controls['number'].setValue('121');
    form.controls['congestionNumber'].setValue(11.11);

    const errorResponse = new HttpErrorResponse({
      error: 'test 500 internal Server',
      status: 500,
      statusText: 'Not Found',
    });
    viaServiceSpy.addVia.and.returnValue(
      throwError(() => {
        const error: any = errorResponse;
        return error;
      })
    );
    component.onAddVia(form.value);
    expect(component.errortest).toContain('Not Found');
    DoneFn();
  });
  it('testing onDeleteVia ', function (DoneFn) {
    const vias: Via = {
      id: 1,
      type: 'Autopista',
      sense: 'Calle',
      number: 121,
      congestionNumber: 11.11,
    };
    viaServiceSpy.deleteVia.and.returnValue(of(vias));
    component.onDeleteVia(vias.id);
    expect(component.errortest).toContain('true');
    const errorResponse = new HttpErrorResponse({
      error: 'test 500 internal Server',
      status: 500,
      statusText: 'Not Found',
    });
    viaServiceSpy.deleteVia.and.returnValue(
      throwError(() => {
        const error: any = errorResponse;
        return error;
      })
    );
    component.onDeleteVia(vias.id);
    expect(component.errortest).toContain('Not Found');
    DoneFn();
  });
  it('testing on updateVia', function (DoneFn) {
    const vias: Via = {
      id: 1,
      type: 'Autopista',
      sense: 'Calle',
      number: 121,
      congestionNumber: 11.11,
    };
    viaServiceSpy.updateVia.and.returnValue(of(vias));
    component.onUpdateVia(vias);
    expect(component.vvTest).toEqual(vias);
    const errorResponse = new HttpErrorResponse({
      error: 'test 500 internal Server',
      status: 500,
      statusText: 'Not Found',
    });
    viaServiceSpy.updateVia.and.returnValue(
      throwError(() => {
        const error: any = errorResponse;
        return error;
      })
    );
    component.onUpdateVia(vias);
    expect(component.errortest).toContain('Not Found');
    DoneFn();
  });
  it('testing search', function (DoneFn) {
    const vias: Via[] = [
      {
        id: 1,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
      {
        id: 2,
        type: 'Carretera Principal',
        sense: 'Calle',
        number: 154,
        congestionNumber: 36.9,
      },
    ];
    viaServiceSpy.getVias.and.returnValue(of(vias));
    component.onSearchVia('Carretera Principal');
    expect(component.viaTest).toEqual(vias);
    DoneFn();
  });
  it('testing modal', function (DoneFn) {
    const vias: Via = {
      id: 1,
      type: 'Autopista',
      sense: 'Calle',
      number: 121,
      congestionNumber: 11.11,
    };
    component.onOpenModal(vias, 'edit');
    expect(component.editVia).toEqual(vias);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    component.onOpenModal(null!, 'add');
    expect(component.errortest).toEqual('false');
    component.onOpenModal(vias, 'delete');
    expect(component.deleteVia).toEqual(vias);
    DoneFn();
  });
});
