import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { ViaComponent } from './via.component';
import { ViaRoutingModule } from './via-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModuleModule } from 'src/app/header/components-module.module';

@NgModule({
  declarations: [ViaComponent],
  imports: [
    CommonModule,
    ViaRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ComponentsModuleModule,
  ],
})
export class ViaModule {}
