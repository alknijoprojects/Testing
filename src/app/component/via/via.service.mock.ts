/* eslint-disable prettier/prettier */
// eslint-disable-next-line prettier/prettier

import { map, Observable, of } from 'rxjs';
import { Via } from 'src/app/model/via';
import { environment } from 'src/environments/environment';


export class ViaServiceMock {
 private apiServerUrl = environment.apiBaseUrl + '/api/Via';

  public getVias(): Observable<Via[]> {
      
    return of([
        {   id: 1,
            type: 'Carretera Principal',
            sense: 'Calle',
            number: 154,
            congestionNumber: 36.9,},
    ]).pipe(map(this.transformViastoVia));
  }

 

  private transformViastoVia(resp: Via[]): Via[] {
    return resp;
  }
}
