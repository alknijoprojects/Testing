import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViaComponent } from './via.component';

const routes: Routes = [
  {
    path: '',
    component: ViaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViaRoutingModule {}
