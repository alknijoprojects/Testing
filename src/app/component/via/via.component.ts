import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Via } from 'src/app/model/via';
import { TokenService } from 'src/app/services/token.service';
import { ViaService } from 'src/app/services/via.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-via',
  templateUrl: './via.component.html',
  styleUrls: ['./via.component.css'],
})
export class ViaComponent implements OnInit {
  isAdmin = false;
  // creacion de variables.
  public vias: Via[] = [];
  public viaTest: Via[] | undefined;
  public vvTest: Via | undefined;
  public viaFilter: Via[] = [];
  public editVia: Via | undefined;
  public deleteVia: Via | undefined;
  public errortest = '';
  viaGroup!: FormGroup;
  //variable para buscar
  public search = '';

  // variables paginacion.
  page = 0;
  pageSize = 5;
  collectionSize = 0;
  page_number = 2;
  pageSizeOptions = [6];

  constructor(
    private viaService: ViaService,
    public formBuilder: FormBuilder,
    private tokenSerivce: TokenService
  ) {
    this.viaGroup = this.formBuilder.group({
      id: [],
      type: [''],
      sense: [''],
      number: [],
      congestionNumber: [],
    });
  }

  ngOnInit(): void {
    this.getVias();
    this.isAdmin = this.tokenSerivce.isAdmin();
  }
  //funciones con la base de datos
  public getVias(): void {
    this.viaService.getVias().subscribe(response => {
      this.viaTest = response;
      this.vias = response;
      this.collectionSize = this.vias.length;
      this.vias = this.vias
        .map((via, i) => ({ counter: i + 1, ...via }))
        .slice(
          (this.page - 1) * this.pageSize,
          (this.page - 1) * this.pageSize + this.pageSize
        );
    });
  }
  public getAllVias(): void {
    this.viaService.getVias().subscribe(response => {
      this.viaFilter = response;
    });
  }
  public onAddVia(via: Via): void {
    const agentform = document.getElementById('add-via-form');
    agentform?.click();
    this.viaService.addVia(via).subscribe({
      next: response => {
        this.vvTest = response;
        this.getVias();
        Swal.fire('Confirmado', 'Via Registrada Exitosamente', 'success');
        this.viaGroup.reset();
      },
      error: error => {
        Swal.fire('Error!', 'La via no se ha logrado crear ', 'error');
        // addForm.reset();
        this.errortest = error.message;
        this.viaGroup.reset();
      },
    });
  }
  public onDeleteVia(viaId: number | any): void {
    this.viaService.deleteVia(viaId).subscribe({
      next: () => {
        this.errortest = 'true';
        this.getVias();
      },
      error: (error: HttpErrorResponse) => {
        this.errortest = error.message;
        Swal.fire('Error!', error.message, 'error');
      },
    });
  }
  public onUpdateVia(via: Via) {
    this.viaService.updateVia(via.id, via).subscribe({
      next: (response: Via) => {
        Swal.fire(
          'Confirmado',
          'La via ' + response.id + ' se actualizo con exito',
          'success'
        );
        this.vvTest = response;
        this.getVias();
      },
      error: (error: HttpErrorResponse) => {
        this.errortest = error.message;
        Swal.fire('Error!', error.message, 'error');
      },
    });
  }

  //funciones para la pagina
  onSearchVia(search: string) {
    this.search = search;
    this.getAllVias();
    //revisar el filtro de via
    const filteredVia = this.viaFilter.filter(
      v =>
        v.id.toString().includes(search) ||
        v.type?.includes(search) ||
        v.sense?.includes(search)
    );
    this.viaTest = filteredVia;
    this.collectionSize = filteredVia.length;
    if (this.search === '') {
      this.errortest = 'true';
      return this.getVias();
    } else {
      this.vias = filteredVia;
      this.viaTest = this.vias;
      this.vias = this.vias
        .map((via, i) => ({ counter: i + 1, ...via }))
        .slice(
          (this.page - 1) * this.pageSize,
          (this.page - 1) * this.pageSize + this.pageSize
        );
      return filteredVia;
    }
  }
  //funcion de los modal
  public onOpenModal(via: Via, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      this.errortest = 'false';
      button.setAttribute('data-target', '#addViaModal');
    }
    if (mode === 'edit') {
      this.editVia = via;
      button.setAttribute('data-target', '#updateViaModal');
    }
    if (mode === 'delete') {
      this.deleteVia = via;
      button.setAttribute('data-target', '#deleteViaModal');
    }
    container?.appendChild(button);
    button.click();
  }
}
