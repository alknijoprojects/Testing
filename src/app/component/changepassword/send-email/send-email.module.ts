import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SendEmailRoutingModule } from './send-email-routing.module';
import { SendEmailComponent } from './send-email.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ComponentsModuleModule } from 'src/app/header/components-module.module';

@NgModule({
  declarations: [SendEmailComponent],
  imports: [
    CommonModule,
    SendEmailRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModuleModule,
    FontAwesomeModule,
    NgbModule,
  ],
})
export class SendEmailModule {}
