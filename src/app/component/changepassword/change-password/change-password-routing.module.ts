import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchangePasswordComponent } from './schange-password.component';

const routes: Routes = [
  {
    path: '',
    component: SchangePasswordComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChangePasswordRoutingModule {}
