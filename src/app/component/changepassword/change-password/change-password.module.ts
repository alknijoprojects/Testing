import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { SchangePasswordComponent } from './schange-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ComponentsModuleModule } from 'src/app/header/components-module.module';
@NgModule({
  declarations: [SchangePasswordComponent],
  imports: [
    CommonModule,
    ChangePasswordRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModuleModule,
    FontAwesomeModule,
    NgbModule,
  ],
})
export class ChangePasswordModule {}
