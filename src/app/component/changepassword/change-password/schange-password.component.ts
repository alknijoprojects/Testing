/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EmailPasswordService } from 'src/app/services/email-password.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangePasswordDTO } from 'src/app/models/change-password-dto';
@Component({
  selector: 'app-schange-password',
  templateUrl: './schange-password.component.html',
  styleUrls: ['./schange-password.component.css'],
})
export class SchangePasswordComponent implements OnInit {
  password!: string;
  confirmpassword!: string;
  tokenPassword!: string;

  dto!: ChangePasswordDTO;

  constructor(
    private emailPasswordService: EmailPasswordService,
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  ngOnInit(): void {}

  onChangePassword(): void {
    if (this.password !== this.confirmpassword) {
      this.toastr.error('Las contraseñas no coinciden', 'Fail', {
        timeOut: 3000,
        positionClass: 'toast-top-center',
      });
      return;
    }
    this.tokenPassword = this.activatedRoute.snapshot.params['tokenPassword'];
    this.dto = new ChangePasswordDTO(
      this.password,
      this.confirmpassword,
      this.tokenPassword
    );
    this.emailPasswordService.changePassword(this.dto).subscribe(
      data => {
        this.toastr.success(data.message, 'OK', {
          timeOut: 3000,
          positionClass: 'toast-top-center',
        });
        this.router.navigate(['/login']);
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {
          timeOut: 3000,
          positionClass: 'toast-top-center',
        });
      }
    );
  }
}
