import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  exports: [MatCardModule, MatPaginatorModule],
})
export class MaterialModule {}
