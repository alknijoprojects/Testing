import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainpageRoutingModule } from './mainpage-routing.module';
import { MainpageComponent } from './mainpage.component';
import { ComponentsModuleModule } from '../header/components-module.module';

@NgModule({
  declarations: [MainpageComponent],
  imports: [ComponentsModuleModule, CommonModule, MainpageRoutingModule],
})
export class MainpageModule {}
