import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './guards/login.guard';
import { ProdGuardService } from './guards/prod-guard.service';

const routes: Routes = [
  { path: 'index', redirectTo: '/index', pathMatch: 'full' },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'via',
    loadChildren: () =>
      import('./component/via/via.module').then(m => m.ViaModule),
    canActivate: [ProdGuardService],
    data: { expectedRol: ['admin', 'user'] },
  },
  {
    path: 'agent',
    loadChildren: () =>
      import('./component/Agent/agent.module').then(m => m.AgentModule),
    canActivate: [ProdGuardService],
    data: { expectedRol: ['admin', 'user'] },
  },
  {
    path: 'change-password/:tokenPassword',
    loadChildren: () =>
      import(
        './component/changepassword/change-password/change-password.module'
      ).then(m => m.ChangePasswordModule),
    canActivate: [LoginGuard],
  },
  {
    path: 'information',
    loadChildren: () =>
      import('./component/information/information.module').then(
        m => m.InformationModule
      ),
    canActivate: [ProdGuardService],
    data: { expectedRol: ['admin', 'user'] },
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./component/auth/login/login.module').then(m => m.LoginModule),
    canActivate: [LoginGuard],
  },
  {
    path: 'sendemail',
    loadChildren: () =>
      import('./component/changepassword/send-email/send-email.module').then(
        m => m.SendEmailModule
      ),
    canActivate: [LoginGuard],
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./component/auth/register/register.module').then(
        m => m.RegisterModule
      ),
    canActivate: [LoginGuard],
  },
  {
    path: 'index',
    loadChildren: () =>
      import('./component/index/index.module').then(m => m.IndexModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
