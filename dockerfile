FROM node:16.14.2-alpine3.14 as builder
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build --prod

FROM nginx:1.21.6-alpine
COPY --from=builder /app/dist/testing /usr/share/nginx/html
EXPOSE 80
CMD ["nginx","-g","daemon off;"]
